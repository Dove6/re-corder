import os
import sys
import socket
import http.client
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import urllib.error
import re
import time
import locale
import inspect
import sqlite3
import json
import collections

#important paths
script_dir = os.path.dirname(os.path.abspath(__file__))
shouts_db_path = os.path.join(script_dir, 'shouts.db')
error_db_path = os.path.join(script_dir, 'error.db')
error_log_path = os.path.join(script_dir, 'error.log')
force_upload_indicator_path = os.path.join(script_dir, 'force_upload.now')

upload_hours = (0, 3, 6, 9, 12, 15, 18, 21)

#try to set Polish locale for Polish timezone
try:
	locale.setlocale(locale.LC_ALL, 'pl')
except locale.Error:
	locale.setlocale(locale.LC_ALL, 'pl_PL.utf8')

sql_error_stmts = {
	'create': '''CREATE TABLE IF NOT EXISTS errors (
			error_id integer PRIMARY KEY,
			filename text,
			line integer,
			content text,
			time text NOT NULL
		)''',
	'insert': '''INSERT INTO errors(filename, line, content, time)
		VALUES(?, ?, ?, datetime('now'))''',
	#delete errors older than 7 days
	'delete': '''DELETE FROM errors
		WHERE time < datetime('now', '-7 days')'''
}
	
def log_error(error, traceback = True, line_number = 0):
	'''
	Logs error to local error database or (in case of database error) to local file. Almost exception-proof.
	:param error: error content (name, short description, etc.)
	:param traceback: flag specifying if function should detect script name and line number; True by default
	:param line_number: line number (used if traceback was set to False); 0 by default
	'''
	try:
		filename = sys.exc_info()[2].tb_frame.f_code.co_filename if traceback else 'script.py'
	except:
		filename = ''
	try:
		line = sys.exc_info()[2].tb_lineno if traceback else line_number
	except:
		line = 0
	try:
		conn = sqlite3.connect(error_db_path)
		cur = conn.cursor()
		cur.execute(sql_error_stmts['create'])
		conn.commit()
		cur.execute(sql_error_stmts['insert'],
			(str(filename),
			int(line),
			str(error)))
		conn.commit()
		cur.execute(sql_error_stmts['delete'])
	except:
		logfile = open(error_log_path, 'a')
		prefix = '[' + filename + ':' + str(line) + '] '
		logfile.write(prefix + str(error) + ' - ' + time.strftime('%d %b %Y %H:%M') + '\n')
		logfile.close()
	finally:
		conn.close()

#chat site connection info
chat_info = {
	'url': 'http://www.przygodyreksia.aidemmedia.pl/pliki/kretes/forum/reksioforum/chat.php',
	'post_data': {'mode': 'read'}
}

#remote database upload site connection info
remote_db_info = {
	'url': 'http://cytaty-z-forum.cba.pl/re-corder/mass_upload.php'
}

regexes = {
	#for parsing chat HTML data
	'primary': re.compile('<div id\="p(\d+)">\s*<table class\="tablebg" cellpadding\="\d+" cellspacing="\d+" width\="\d+%">\s*(<tbody>)?\s*<tr class\="\w+">\s*<td>\s*(<a .*? title\="Usuń post".*?<\/a>\s*)?<b class="postauthor"><a href="((\.)|(http:\/\/.*?reksioforum))\/memberlist\.php\?mode\=viewprofile(&amp;)?u\=(\d+)(&amp;sid\=\w+)?"( style\="color: #(\w{6});" class\="username\-coloured")?>(.*?)<\/a><\/b> « ([\w\s,:]+?) » &nbsp; <span class\="postbody">([\s\S]*?)<\/span>\s*<\/td>\s*<\/tr>\s*(<\/tbody>)?<\/table>\s*<\/div>'),
	#for replacing unwanted scripts with their BBcodes
	'alert': re.compile('<script type\="text\/javascript">\s*?alert\("(.*?)"\)\s*?<\/script>'),
	'apo': re.compile('<script type\="text\/javascript" src\="http:\/\/i2\.pinger\.pl\/pgr373\/f52b496900277c7e4ef98aa3><\/script>'),
	'snow': re.compile('<script type\="text\/javascript" src\="http:\/\/404bajery\.pl\/pada\/custom\.php\?flake\=6">[\s\S]*?alt="snieg" style\="position:absolute; top: 0px; left: 0px;"( \/)?><\/a>')
}

'''
Column name			Corresponding primary regex group
message_id  		1
chat_id 			1
user_id group   	4
username group  	8
user_colour group	7
message group   	10
bbcode_bitfield 	-
bbcode_uid  		-
bbcode_options		-
time group			9
'''

sql_shouts_stmts = {
	'create': '''CREATE TABLE IF NOT EXISTS shouts (
			message_id integer PRIMARY KEY,
			chat_id integer NOT NULL,
			user_id integer NOT NULL,
			username text NOT NULL,
			user_colour text,
			message text,
			time integer NOT NULL
		);''',
	'insert': '''INSERT INTO shouts(message_id, chat_id, user_id, username, user_colour, message, time)
		VALUES(?, ?, ?, ?, ?, ?, ?)''',
	'delete': 'DELETE FROM shouts',
	'select': '''SELECT * FROM shouts
		ORDER BY message_id'''
}
	
def insert_local(data):
	'''
	Inserts shouts into local sqlite database.
	:param data: dictionary containing columns' names and values (must have length of 7)
	'''
	if len(data) != 7:
		raise AssertionError('Invalid data list length: ' + str(len(data)))
	try:
		conn = sqlite3.connect(shouts_db_path)
		cur = conn.cursor()
		cur.execute(sql_shouts_stmts['create'])
		conn.commit()
		cur.execute(sql_shouts_stmts['insert'],
			(int(data['message_id']),
			int(data['chat_id']),
			int(data['user_id']),
			str(data['username']),
			str(data['user_colour']),
			str(data['message']),
			int(data['time'])))
		conn.commit()
	finally:
		conn.close()
		
def clear_local():
	'''
	Deletes all shouts from local sqlite database.
	'''
	try:
		conn = sqlite3.connect(shouts_db_path)
		cur = conn.cursor()
		cur.execute(sql_shouts_stmts['delete'])
		conn.commit()
	finally:
		conn.close()

def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d

def upload_from_local():
	'''
	Uploads all shouts from local sqlite database to server.
	'''
	try:
		conn = sqlite3.connect(shouts_db_path)
		conn.row_factory = dict_factory
		cur = conn.cursor()
		cur.execute(sql_shouts_stmts['select'])
		shouts = cur.fetchall()
		if len(shouts) > 0:
			req = Request(remote_db_info['url'])
			req.add_header('Content-Type', 'application/json; charset=utf-8')
			data = json.dumps(shouts).encode('utf-8')
			req.add_header('Content-Length', len(data))
			store_response = urlopen(req, data, 10)
			if store_response.getcode() == 200:
				print('Shouts uploaded, printing results:')
				response_data = store_response.read().decode()
				response_json = json.loads(response_data)
				if len(response_json) != len(shouts):
					raise AssertionError('Number of sent shouts (' + str(len(shouts)) + ') \
						not equal to number of response codes (' + str(len(response_json)) + ')')
				response_json = collections.OrderedDict(sorted(response_json.items()))
				for id, code in response_json.items():
					print(str(id) + ': ', end='')
					if code == 200:
						print('OK')
					elif code == 400:
						print('missing data')
					elif code == 409:
						print('already present')
					elif code == 500:
						print('unknown insertion error')
			else:
				print('Error uploading shouts, but exception has not been raised (code: ' + str(store_response.getcode()) + ')')
		else:
			print('No shouts to send')
	finally:
		conn.close()

last_id = -1
has_uploaded = False

while True:
	#get chat data
	chat_response = '---'
	try:
		chat_response = urlopen(chat_info['url'], urlencode(chat_info['post_data']).encode(), 3).read().decode()
	except urllib.error.HTTPError as err:
		print('HTTP error (code: ' + str(err.code) + ')')
		log_error('urllib.error.HTTPError: ' + str(err.code))
	except urllib.error.URLError:
		print('Something is wrong with the first URL')
		log_error('urllib.error.URLError')
	except http.client.HTTPException:
		print('HTTP not in mood today')
		log_error('http.client.HTTPException')
	except socket.timeout:
		print('HTTP request timeout')
		log_error('socket.timeout')
	except ConnectionResetError:
		print('Connection VIOLENTLY reset')
		log_error('ConnectionResetError')
	except KeyboardInterrupt:
		print('Traceback (most recent call last):')
		print('  File "script.py", line ' + str(sys.exc_info()[2].tb_lineno) + ', in <module>')
		print('    store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()')
		print('KeyboardInterrupt')
		exit()
	except Exception as exception:
		print('Something else: ' + exception.__class__.__name__)
		log_error(exception.__class__.__name__)
		
	#process chat data
	chat_response = chat_response.split('---')[0]
	shouts = regexes['primary'].findall(chat_response)
	shouts.reverse()
	for i in range(len(shouts)):
		#prepare single shout dictionary
		data = {
			'message_id': list(shouts[i])[0],
			'chat_id': 1,
			'user_id': list(shouts[i])[7],
			'username': list(shouts[i])[11],
			'user_colour': list(shouts[i])[10],
			'message': regexes['apo'].sub('[apo][/apo]',
					   regexes['snow'].sub('[snieggg][/snieggg]',
					   regexes['alert'].sub('[komunikat2]\g<1>[/komunikat2]', list(shouts[i])[13]))),
			'time': int(str(time.mktime(time.strptime(list(shouts[i])[12].replace('maja', 'maj'), '%d %b %Y %H:%M'))).split('.')[0]) + \
				(3600 if bool(time.localtime().tm_isdst) else 0)
		}
		
		#insert the shout into the local database
		if int(data['message_id']) > last_id:
			try:
				insert_local(data)
				last_id = int(data['message_id'])
				print('Local insertion completed (message_id: ' + data['message_id'] + ')')
			except sqlite3.IntegrityError as e:
				if repr(e).find('UNIQUE constraint failed: shouts.message_id') != -1:
					last_id = int(data['message_id'])
					print('Shout already in local database (message_id: ' + data['message_id'] + ')')
				else:
					print(e)
					log_error(e)
				
	#upload local shouts to server		
	'''
    hour = time.localtime().tm_hour
	try:
		if hour in upload_hours:
			if has_uploaded == False:
				upload_from_local()
				clear_local()
				has_uploaded = True
		else:
			has_uploaded = False
		if os.path.exists(force_upload_indicator_path):
			upload_from_local()
			clear_local()
			os.remove(force_upload_indicator_path)
	except json.JSONDecodeError as err:
		print('Invalid server response data')
		log_error(repr(err))			
	except sqlite3.IntegrityError as e:
		print(e)
		log_error(e)
	except urllib.error.HTTPError as err:
		if err.code == 400:
			print('Invalid or missing POST data')
		elif err.code == 500:
			print('Internal server error')
		elif err.code == 418:
			print('             ;,\'')
			print('     _o_    ;:;\'')
			print(' ,-.\'---`.__ ;')
			print('((j`=====\',-\'')
			print(' `-\ hjw /')
			print('    `-=-\'')
			print('Server is mad')
		else:
			print('HTTP header is not OK')
		log_error('urllib.error.HTTPError: ' + str(err.code))
	except urllib.error.URLError:
		print('Something is wrong with the second URL')
		log_error('urllib.error.URLError')
	except http.client.HTTPException:
		print('HTTP not in mood today')
		log_error('http.client.HTTPException')
	except socket.timeout:
		print('HTTP request timeout')
		log_error('socket.timeout')
	except ConnectionResetError:
		print('Connection VIOLENTLY reset')
		log_error('ConnectionResetError')
	except KeyboardInterrupt:
		print('Traceback (most recent call last):')
		print('  File "script.py", line ' + str(sys.exc_info()[2].tb_lineno) + ', in <module>')
		print('    store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()')
		print('KeyboardInterrupt')
		exit()
	except Exception as exception:
		print('Something else: ' + exception.__class__.__name__)
		log_error(exception.__class__.__name__)
		raise
    '''
		
	#cooldown
	time.sleep(2)