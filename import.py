print("Deprecated: use mass_import.py instead")
exit()

import sys
import os
import socket
import http.client
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import urllib.error
import re
import time
import locale
try:
	locale.setlocale(locale.LC_ALL, 'pl')
except locale.Error:
	locale.setlocale(locale.LC_ALL, 'pl_PL.utf8')

def log_error(error_name, line = -1):
	logfile = open(os.getcwd() + '\\error.log', 'a')
	if (line < 1):
		logfile.write(str(error_name) + ' - ' + time.strftime('%d %b %Y %H:%M') + '\n')
	else:
		logfile.write(str(line) + ': ' + str(error_name) + ' - ' + time.strftime('%d %b %Y %H:%M') + '\n')
	logfile.close()

if len(sys.argv) == 3:
	try:
		pro_start = int(sys.argv[1])
		pro_stop = int(sys.argv[2])
	except ValueError:
		print("Usage: import.py start_number end_number")
		exit()
elif len(sys.argv) == 2:
	try:
		pro_start = int(sys.argv[1])
		pro_stop = int(sys.argv[1])
	except ValueError:
		print("Usage: import.py start_number end_number")
		exit()
else:
	print("Usage: import.py start_number end_number")
	exit()

store_url = 'http://cytaty-z-forum.cba.pl/re-corder/upload.php'

ultimate_post_regex = re.compile('<div id\="p(\d+)">\s*<table class\="tablebg" cellpadding\="\d+" cellspacing="\d+" width\="\d+%">\s*(<tbody>)?\s*<tr class\="\w+">\s*<td>\s*<b class="postauthor"><a href="((\.)|(http:\/\/.*?reksioforum))\/memberlist\.php\?mode\=viewprofile(&amp;)?u\=(\d+)(&amp;sid\=\w+)?"( style\="color: #(\w{6});" class\="username\-coloured")?>(.*?)<\/a><\/b> � ([\w\s:]+?) � &nbsp; <span class\="postbody">([\s\S]*?)<\/span>\s*<\/td>\s*<\/tr>\s*(<\/tbody>)?<\/table>\s*<\/div>')
alert_regex = re.compile('<script type\="text\/javascript">\s*?alert\("(.*?)"\)\s*?<\/script>')
snow_regex = re.compile('<script type\="text\/javascript" src\="http:\/\/404bajery\.pl\/pada\/custom\.php\?flake\=6">[\s\S]*?alt="snieg" style\="position:absolute; top: 0px; left: 0px;"( \/)?><\/a>')
apo_regex = re.compile('<script type\="text\/javascript" src\="http:\/\/i2\.pinger\.pl\/pgr373\/f52b496900277c7e4ef98aa3><\/script>')

#message_id group 1
#chat_id = 1
#user_id group 4
#username group 8
#user_colour group 7
#message group 10
#bbcode_bitfield//
#bbcode_uid//
#bbcode_options//
#time group 9

for n in range(pro_start, pro_stop + 1):
	crawl_url = 'http://cytaty-z-forum.cba.pl/pro/nope' + str(n) + '.html'
	download_success = False
	while download_success != True:
		try:
			crawl_response = urlopen(crawl_url, None, 3).read().decode()
			download_success = True
		except urllib.error.HTTPError as err:
			print(str(n) + ': Nice fresh HTTP error (code: ' + str(err.code) + ')')
			log_error(str(n) + ': urllib.error.HTTPError: ' + str(err.code), 51)
		except urllib.error.URLError:
			print(str(n) + ': Something is wrong with the first URL')
			log_error(str(n) + ': urllib.error.URLError', 51)
		except http.client.HTTPException:
			print(str(n) + ': HTTP not in mood today')
			log_error(str(n) + ': http.client.HTTPException', 51)
		except socket.timeout:
			print(str(n) + ': But you acted too slow and you ran out of time...')
			log_error(str(n) + ': socket.timeout', 51)
		except ConnectionResetError:
			print(str(n) + ': Connection VIOLENTLY reset')
			log_error(str(n) + ': ConnectionResetError', 51)
		except KeyboardInterrupt:
			print('Traceback (most recent call last):')
			print('  File "script.py", line 56, in <module>')
			print('    store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()')
			print('KeyboardInterrupt', 51)
			exit()
		except Exception as exception:
			print(str(n) + ': Something else: ' + exception.__class__.__name__)
			log_error(str(n) + ': ' + exception.__class__.__name__, 51)

	shouts = ultimate_post_regex.findall(crawl_response)
	shouts.reverse()
	for i in range(len(shouts)):
		data = {'message_id': list(shouts[i])[0], 'chat_id': 1, 'user_id': list(shouts[i])[6], 'username': list(shouts[i])[10], 'user_colour': list(shouts[i])[9], 'message': apo_regex.sub('[apo][/apo]', snow_regex.sub('[snieggg][/snieggg]', alert_regex.sub('[komunikat2]\g<1>[/komunikat2]', list(shouts[i])[12]))), 'time': int(str(time.mktime(time.strptime(list(shouts[i])[11], '%d %b %Y %H:%M'))).split('.')[0]) + 3600}
		upload_success = False
		while upload_success != True:
			try:
				store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()
				if store_response == 200:
					print(str(n) + ': Everything is in order (message_id: ' + data['message_id'] + ')')
					upload_success = True
				else:
					print(str(n) + ': Literally what the fuck ' + store_response + ' (message_id: ' + data['message_id'] + ')')
			except urllib.error.HTTPError as err:
				if err.code == 409:
					print(str(n) + ': Shout already in database (message_id: ' + data['message_id'] + ')')
					upload_success = True
				elif err.code == 400:
					print(str(n) + ': Invalid or missing POST data (message_id: ' + data['message_id'] + ')')
				elif err.code == 500:
					print(str(n) + ': Server has a breakdown right now (message_id: ' + data['message_id'] + ')')
				elif err.code == 418:
					print(str(n) + ': Server is mad at you (message_id: ' + data['message_id'] + ')')
				else:
					(str(n) + ': HTTP header is not OK. h e h e')
				if err.code != 409:
					log_error(str(n) + ': urllib.error.HTTPError: ' + str(err.code), 84)
			except urllib.error.URLError:
				print(str(n) + ': Something is wrong with the second URL')
				log_error(str(n) + ': urllib.error.URLError', 84)
			except http.client.HTTPException:
				print(str(n) + ': HTTP not in mood today')
				log_error(str(n) + ': http.client.HTTPException', 84)
			except socket.timeout:
				print(str(n) + ': But you acted too slow and you ran out of time...')
				log_error(str(n) + ': socket.timeout', 84)
			except ConnectionResetError:
				print(str(n) + ': Connection VIOLENTLY reset')
				log_error(str(n) + ': ConnectionResetError', 84)
			except KeyboardInterrupt:
				print('Traceback (most recent call last):')
				print('  File "script.py", line 84, in <module>')
				print('    store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()')
				print('KeyboardInterrupt')
				exit()
			except Exception as exception:
				print(str(n) + ': Something else: ' + exception.__class__.__name__)
				log_error(str(n) + ': ' + exception.__class__.__name__, 84)
	time.sleep(1)