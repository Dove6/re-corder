import sys
import os
import socket
import http.client
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import urllib.error
import json
import re
import time
import locale
import inspect
try:
	locale.setlocale(locale.LC_ALL, 'pl')
except locale.Error:
	locale.setlocale(locale.LC_ALL, 'pl_PL.utf8')

def lineno():
	return inspect.currentframe().f_back.f_lineno

def log_error(error_name, traceback = True, line_number = 0):
	prefix = '[' + (sys.exc_info()[2].tb_frame.f_code.co_filename if traceback else 'mass_import.py') + ':' + str((sys.exc_info()[2].tb_lineno if traceback else line_number)) + '] '
	logfile = open(os.getcwd() + '\\error.log', 'a')
	logfile.write(prefix + str(error_name) + ' - ' + time.strftime('%d %b %Y %H:%M') + '\n')
	logfile.close()
	
def replace_full_month(date_string):
	return date_string.replace('stycznia', 'styczeń').replace('lutego', 'luty').replace('marca', 'marzec').replace('kwietnia', 'kwiecień').replace('maja', 'maj').replace('czerwca', 'czerwiec').replace('lipca', 'lipiec').replace('sierpnia', 'sierpień').replace('września', 'wrzesień').replace('października', 'październik').replace('listopada', 'listopad').replace('grudnia', 'grudzień')

def replace_short_day(date_string):
	return date_string.replace('Pn', 'pon', 1).replace('Wt', 'wto', 1).replace('Śr', 'śro', 1).replace('Cz', 'czw', 1).replace('Pt', 'pią', 1).replace('So', 'sob', 1).replace('N', 'nie', 1)

if len(sys.argv) == 4:
	try:
		pro_start = int(sys.argv[1])
		pro_stop = int(sys.argv[2])
		part_size = int(sys.argv[3])
	except ValueError:
		print("Usage: import.py [start_]number [end_number] [shouts_at_once]")
		exit()
elif len(sys.argv) == 3:
	try:
		pro_start = int(sys.argv[1])
		pro_stop = int(sys.argv[2])
		part_size = 400
	except ValueError:
		print("Usage: import.py [start_]number [end_number] [shouts_at_once]")
		exit()
elif len(sys.argv) == 2:
	try:
		pro_start = int(sys.argv[1])
		pro_stop = int(sys.argv[1])
		part_size = 400
	except ValueError:
		print("Usage: import.py [start_]number [end_number] [shouts_at_once]")
		exit()
else:
	print("Usage: import.py [start_]number [end_number] [shouts_at_once]")
	exit()

store_url = 'http://cytaty-z-forum.cba.pl/re-corder/mass_upload.php'

ultimate_post_regex = re.compile('<div id\="p(\d+)">\s*<table class\="tablebg" cellpadding\="\d+" cellspacing="\d+" width\="\d+%">\s*(<tbody>)?\s*<tr class\="\w+">\s*<td>\s*(<a .*? title\="Usuń post".*?<\/a>\s*)?<b class="postauthor"><a href="((\.)|(http:\/\/.*?reksioforum))\/memberlist\.php\?mode\=viewprofile(&amp;)?u\=(\d+)(&amp;sid\=\w+)?"( style\="color: #(\w{6});" class\="username\-coloured")?>(.*?)<\/a><\/b> « ([\w\s,:]+?) » &nbsp; <span class\="postbody">([\s\S]*?)<\/span>\s*<\/td>\s*<\/tr>\s*(<\/tbody>)?<\/table>\s*<\/div>')
alert_regex = re.compile('<script type\="text\/javascript">\s*?alert\("(.*?)"\)\s*?<\/script>')
snow_regex = re.compile('<script type\="text\/javascript" src\="http:\/\/404bajery\.pl\/pada\/custom\.php\?flake\=6">[\s\S]*?alt="snieg" style\="position:absolute; top: 0px; left: 0px;"( \/)?><\/a>')
apo_regex = re.compile('<script type\="text\/javascript" src\="http:\/\/i2\.pinger\.pl\/pgr373\/f52b496900277c7e4ef98aa3><\/script>')

#message_id group 1
#chat_id = 1
#user_id group 8
#username group 12
#user_colour group 11
#message group 14
#bbcode_bitfield//
#bbcode_uid//
#bbcode_options//
#time group 13

for n in range(pro_start, pro_stop + 1):
	crawl_url = 'http://cytaty-z-forum.cba.pl/pro/nope' + str(n) + '.html'
	download_success = False
	while download_success != True:
		try:
			crawl_response = urlopen(crawl_url, None, 60).read().decode()
			download_success = True
		except urllib.error.HTTPError as err:
			print(str(n) + ': Nice fresh HTTP error (code: ' + str(err.code) + ')')
			log_error(str(n) + ': urllib.error.HTTPError: ' + str(err.code))
		except urllib.error.URLError:
			print(str(n) + ': Something is wrong with the first URL')
			log_error(str(n) + ': urllib.error.URLError')
		except http.client.HTTPException:
			print(str(n) + ': HTTP not in mood today')
			log_error(str(n) + ': http.client.HTTPException')
		except socket.timeout:
			print(str(n) + ': But you acted too slow and you ran out of time...')
			log_error(str(n) + ': socket.timeout')
		except ConnectionResetError:
			print(str(n) + ': Connection VIOLENTLY reset')
			log_error(str(n) + ': ConnectionResetError')
		except KeyboardInterrupt:
			print('Traceback (most recent call last):')
			print('  File "mass_import.py", line ' + str(sys.exc_info()[2].tb_lineno) + ', in <module>')
			print('    store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()')
			print('KeyboardInterrupt')
			exit()
		except UnicodeDecodeError:
			crawl_response = urlopen(crawl_url, None, 60).read().decode('cp1250')
			download_success = True
	print(str(n) + ': Preparing shouts...')
	shouts = ultimate_post_regex.findall(crawl_response)
	shouts.reverse()
	shouts_count = len(shouts)
	part = 0
	for i in range(len(shouts)):
		try:
			shouts[i] = {'message_id': list(shouts[i])[0], 'chat_id': 1, 'user_id': list(shouts[i])[7], 'username': list(shouts[i])[11], 'user_colour': list(shouts[i])[10], 'message': apo_regex.sub('[apo][/apo]', snow_regex.sub('[snieggg][/snieggg]', alert_regex.sub('[komunikat2]\g<1>[/komunikat2]', list(shouts[i])[13]))), 'time': int(str(time.mktime(time.strptime(list(shouts[i])[12].replace('maja', 'maj'), '%d %b %Y %H:%M'))).split('.')[0]) + 3600}
		except ValueError:
			try:
				shouts[i] = {'message_id': list(shouts[i])[0], 'chat_id': 1, 'user_id': list(shouts[i])[7], 'username': list(shouts[i])[11], 'user_colour': list(shouts[i])[10], 'message': apo_regex.sub('[apo][/apo]', snow_regex.sub('[snieggg][/snieggg]', alert_regex.sub('[komunikat2]\g<1>[/komunikat2]', list(shouts[i])[13]))), 'time': int(str(time.mktime(time.strptime(list(shouts[i])[12].replace('maja', 'maj'), '%a, %d %b %Y, %H:%M'))).split('.')[0]) + 3600}
			except ValueError:
				try:
					shouts[i] = {'message_id': list(shouts[i])[0], 'chat_id': 1, 'user_id': list(shouts[i])[7], 'username': list(shouts[i])[11], 'user_colour': list(shouts[i])[10], 'message': apo_regex.sub('[apo][/apo]', snow_regex.sub('[snieggg][/snieggg]', alert_regex.sub('[komunikat2]\g<1>[/komunikat2]', list(shouts[i])[13]))), 'time': int(str(time.mktime(time.strptime(replace_short_day(list(shouts[i])[12]).replace('maja', 'maj'), '%a, %d %b %Y, %H:%M'))).split('.')[0]) + 3600}
				except ValueError:
					shouts[i] = {'message_id': list(shouts[i])[0], 'chat_id': 1, 'user_id': list(shouts[i])[7], 'username': list(shouts[i])[11], 'user_colour': list(shouts[i])[10], 'message': apo_regex.sub('[apo][/apo]', snow_regex.sub('[snieggg][/snieggg]', alert_regex.sub('[komunikat2]\g<1>[/komunikat2]', list(shouts[i])[13]))), 'time': int(str(time.mktime(time.strptime(replace_full_month(list(shouts[i])[12]), '%A, %d %B %Y, %H:%M'))).split('.')[0]) + 3600}
	print(str(n) + ': Total ' + str(shouts_count) + ' shouts...')
	while part * part_size < shouts_count:
		shouts_part = shouts[part * part_size : ((part + 1) * part_size if (part + 1) * part_size <= shouts_count else shouts_count)]
		shouts_part_count = len(shouts_part)
		shouts_part = json.dumps(shouts_part).encode('utf-8')
		print(str(n) + ': Uploading ' + str(shouts_part_count) + ' shouts...')
		upload_success = False
		while upload_success != True:
			try:
				json_request = Request(store_url)
				json_request.add_header('Content-Type', 'application/json; charset=utf-8')
				store_response = urlopen(store_url, shouts_part, 300).read().decode()
				try:
					json_store_response = dict(json.loads(store_response))
				except json.decoder.JSONDecodeError:
					json_store_response = {}
				upload_success = True
				if len(store_response) > 0:
					duplicate_count = sum(1 for x in json_store_response.values() if x == 409)
					print(str(n) + ': ' + str(shouts_part_count) + ' shouts uploaded with ' + str(duplicate_count) + ' duplicate errors and ' + str(len(json_store_response) - duplicate_count) + ' fatal errors')
					response_keys = list(json_store_response.keys())
					response_keys = list(map(int, response_keys))
					response_keys.sort()
					for id in response_keys:
						log_error('[' + str(id) + '] ' + ('Duplicate' if json_store_response[str(id)] == 409 else 'Server error'), False, lineno())
				else:
					print(str(n) + ': Everything is in order')
			except urllib.error.HTTPError as err:
				if err.code == 400:
					print(str(n) + ': Invalid or missing POST data')
				elif err.code == 500:
					print(str(n) + ': Server has a breakdown right now')
				elif err.code == 418:
					print(str(n) + ': Server is mad at you')
				else:
					(str(n) + ': HTTP header is not OK. h e h e')
				log_error(str(n) + ': urllib.error.HTTPError: ' + str(err.code))
			except urllib.error.URLError:
				print(str(n) + ': Something is wrong with the second URL')
				log_error(str(n) + ': urllib.error.URLError')
			except http.client.HTTPException:
				print(str(n) + ': HTTP not in mood today')
				log_error(str(n) + ': http.client.HTTPException')
			except socket.timeout:
				print(str(n) + ': But you acted too slow and you ran out of time...')
				log_error(str(n) + ': socket.timeout')
			except ConnectionResetError:
				print(str(n) + ': Connection VIOLENTLY reset')
				log_error(str(n) + ': ConnectionResetError')
			except KeyboardInterrupt:
				print('Traceback (most recent call last):')
				print('  File "mass_import.py", line ' + str(sys.exc_info()[2].tb_lineno) + ', in <module>')
				print('    store_response = urlopen(store_url, urlencode(data).encode(), 3).getcode()')
				print('KeyboardInterrupt')
				exit()
			'''except Exception as exception:
				print(str(n) + ': Something else: ' + exception.__class__.__name__)
				log_error(str(n) + ': ' + exception.__class__.__name__, 84)
				print(store_response)'''
		part += 1
		logfile = open(os.getcwd() + '\\last.log', 'w')
		logfile.write(str(n) + '\n')
		logfile.close()
		time.sleep(1)
